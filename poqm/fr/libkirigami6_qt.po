# Vincent Pinon <vpinon@kde.org>, 2016, 2017.
# Simon Depiets <sdepiets@gmail.com>, 2018, 2019.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2022-12-22 18:50+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: qtrich\n"
"X-Qt-Contexts: true\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Envoyer un courriel à %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Soyez impliqué"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr "Faire un don"

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Signaler un bogue"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Droit d'auteur"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Licence :"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Licence : %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Bibliothèques en cours d'utilisation"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Auteurs"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Afficher les photos des auteurs"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Crédits"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Traducteurs"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "À propos de %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Quitter"

#: controls/ActionToolBar.qml:193
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Plus d'actions"

#: controls/Avatar.qml:182
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "Supprimer une étiquette"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Actions"

#: controls/GlobalDrawer.qml:506
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Retour"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Fermer la barre latérale"

#: controls/GlobalDrawer.qml:602
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Ouvrir une barre latérale"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "Chargement en cours..."

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Mot de passe"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Fermer le menu"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "Ouvrir le menu"

#: controls/SearchField.qml:94
msgctxt "SearchField|"
msgid "Search…"
msgstr "Rechercher…"

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr "Rechercher"

#: controls/SearchField.qml:107
msgctxt "SearchField|"
msgid "Clear search"
msgstr "Effacer une recherche"

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Configuration"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Configuration — %1"

#. Accessibility text for a page tab. Keep the text as concise as possible and don't use a percent sign.
#: controls/swipenavigator/templates/PageTab.qml:38
#, qt-format
msgctxt "PageTab|"
msgid "Current page. Progress: %1 percent."
msgstr "Page actuelle. Avancement : %1 pourcent."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:41
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Progress: %2 percent."
msgstr "Naviguer vers %1. Avancement : %2 pourcent."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:46
msgctxt "PageTab|"
msgid "Current page."
msgstr "Page actuelle."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:49
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Demanding attention."
msgstr "Naviguer vers %1. Attention requise."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:52
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1."
msgstr "Navigation vers %1"

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Fermer un tiroir"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr "Ouvrir un tiroir"

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Navigation arrière"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Navigation avant"

#: controls/ToolBarApplicationHeader.qml:89
msgctxt "ToolBarApplicationHeader|"
msgid "More Actions"
msgstr "Actions supplémentaires"

#: controls/UrlButton.qml:51
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "Copier un lien dans le presse-papier"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "Le système de fenêtres %1"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (compilé avec %3)"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "Visiter la page de la boutique de KDE pour %1"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Copier l'adresse du lien"

#~ msgctxt "LoadingPlaceholder|"
#~ msgid "Still loading, please wait."
#~ msgstr "Encore en cours de téléchargement, veuillez patienter."

#~ msgctxt "AboutItem|"
#~ msgid "(%1)"
#~ msgstr "(%1)"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Chercher…"

#~ msgctxt "AboutPage|"
#~ msgid "%1 <%2>"
#~ msgstr "%1 <%2>"

#~ msgctxt "ToolBarPageHeader|"
#~ msgid "More Actions"
#~ msgstr "Actions supplémentaires"
